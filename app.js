"use strict";
const dotenv = require("dotenv").config();
const Discord = require("discord.js");
const request = require("./googlesheetsrequest");
const fs = require("fs");
const bot = new Discord.Client();

let cd = new Set();
let prefix = process.env.PREFIX;
var commands = require("./commands.js");

let guildOwner;

bot.on("ready", () => {
    console.log(`I'm online: ${bot.user.presence.status}`);
    bot.user.setActivity("user's membership", { type: "WATCHING" });
});

bot.on("guildCreate", guild => guildOwner = guild.owner);

bot.on("message", msg => {
    if(msg.author.bot && !msg.content.startsWith(prefix)) return;
    else if(cd.has(msg.author.id)) 
    {
        msg.channel.send("Command on cooldown!");
        return;
    }

    let args = msg.content.slice(prefix.length).trim().split(/ +/g),
        cmd = args.shift().toLowerCase();

    if(commands[cmd] !== undefined) commands[cmd](msg, bot, args);

    cd.add(msg.author.id);
    bot.setTimeout( () => {
        cd.delete(msg.author.id);
    }, 3000);
});

bot.on("raw", () => {
    bot.setTimeout( () => {
        console.log(new Date());
        request( (err, res) => {
            if(err) 
            {
                console.error(err);
                return;
            }

            let data = res.data.values;
            data.map( col => {
                if(col[1] <= 3) // Less than or equal to 3 days
                {
                    let user = bot.users.find("tag", col[0]);
                    if(user === undefined) console.log("User not found!!!", col[0]);

                    let embed = new Discord.RichEmbed()
                        .setColor([255, 10, 10]).setTitle(`You have ${col[1]} days left on your membership!`).setDescription("Please remember to renew at <insert link>")
                        .setTimestamp(new Date());

                    if(user !== undefined)
                        user.send(embed);

                    //Sends a reminder to guild owner, might need his client id for this
                    //let guildOwner = "200042113814102016";
                    //bot.fetchUser(guildOwner).then( owner => owner.send(`Notifs: ${col[0]} has ${col[1]} days left of his membership`)).catch(console.error);

                    //Actually, will need guild.owner for this
                    if(guildOwner !== null) guildOwner.send(`Notifs: ${col[0]} has ${col[1]} days left of his membership`);
                }
            });
        });
    }, 1000);//4 * 60 * 60 * 1000);
});

bot.login(process.env.TOKEN);