const fs = require("fs");
const {google} = require("googleapis");
const SheetsID = process.env.SHEETSID;
const token = JSON.parse(fs.readFileSync("./token.json"));
const keys = JSON.parse(fs.readFileSync(`./credentials.json`));

//=================DEBUGGING PURPOSES ONLY - DO NOT USE IN PRODUCTION==========================
function generateNewToken(auth)
{
    console.log("Authorizing a new OAuth2 URL and generating a token...");
    const readline = require("readline");
    const SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly'];
    console.log(`Visit this page to recieve the code: ${auth.generateAuthUrl({ access_type: 'offline', scope: SCOPES })}`);

    let rl = readline.createInterface({
       input: process.stdin,
       output: process.stdout 
    });
    rl.question("Enter the code recieved from website here: ", code => {
        rl.close();

        auth.getToken(code, (err, token) => {
            if (err) return;
            auth.setCredentials(token);
    
            // Store the token to disk for later program executions
            fs.writeFile("./token.json", JSON.stringify(token), err => {
                if (err) return; //console.error(err);
                console.log('Token stored to token.json');
            });
        });
    });
}
//=================DEBUGGING PURPOSES ONLY - DO NOT USE IN PRODUCTION==========================

module.exports = callback => {
    const client = new google.auth.OAuth2(
        keys.web.client_id,
        keys.web.client_secret,
        `http://localhost:8080`
    );

    // If error, comment out the next few lines of code and use generateNewToken(auth) function to reset the authentication options
    // NOTE: Should only be used in debugging

    client.setCredentials(token);

    const sheets = google.sheets({ version: `v4` });
    sheets.spreadsheets.values.get({
        auth: client,
        spreadsheetId: SheetsID,
        range: `Data!A2:B`
    }, (err, res) => callback(err, res));
}

/*
function authorize()
{
    const keys = JSON.parse(fs.readFileSync(`./credentials.json`));
    const SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly'];
    const client = new google.auth.OAuth2(
        keys.web.client_id,
        keys.web.client_secret,
        `http://localhost:8080`
    );

    //console.log(client.generateAuthUrl({ access_type: 'offline', scope: SCOPES }));

    let TOKEN_PATH = "./token.json";
    client.setCredentials(JSON.parse(fs.readFileSync(TOKEN_PATH)));

    // client.getToken(process.env.CODE, (err, token) => {
    //   if (err) return; //console.log(err);
    //   client.setCredentials(token);

    //   // Store the token to disk for later program executions
    //   fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
    //     if (err) return; //console.error(err);
    //     console.log('Token stored to', TOKEN_PATH);
    //   });
    // });

    const sheets = google.sheets({version: 'v4'});
    sheets.spreadsheets.values.get({
        auth: client,
        spreadsheetId: '1pUhWaQISZa7dzLe8rZjW3UPrFxCa20RP1PrMOr-hLPs',
        range: 'Data!A2:C',
    }, (err, res) => {
        if (err) return console.log('The API returned an error: ' + err);

        const rows = res.data.values;
        if (rows.length) 
        {
            console.log('Name, Major:');

            rows.map((row) => console.log(`${row[0]}, ${row[1]}`));
        } 
        else 
        {
            console.log('No data found.');
        }
    });
}
*/