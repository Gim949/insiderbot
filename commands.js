const { RichEmbed } = require("discord.js");
const request = require("./googlesheetsrequest.js");

module.exports = {
    "vip": (msg, bot, args) => {
        let embed = new RichEmbed().setAuthor(bot.user.username, bot.user.avatarURL).setColor([255, 125, 15])
        .setDescription(`In order to access the server, you will need to purchase membership here: <insert link>\nUse ${process.env.PREFIX}myinfo to view your membership status.`)
        .setTimestamp(new Date());

        msg.author.send(embed);
    },

    "myinfo": (msg, bot, args) => {
        try
        {
            request( (err, res) => {
                if(err) console.log(err);

                //Format: [ [Discord tag, time left], ...]
                let userData = eval(JSON.stringify(res.data.values));
                let timeLeft = undefined;

                userData.map( col => {
                    if(col[0] !== undefined && col[1] !== undefined && msg.author.tag == col[0])
                        timeLeft = col[1];
                });

                if(timeLeft === undefined)
                    msg.author.send(`!!! No membership status found. Contact the owner or apply for membership at <insert link> !!!`);
                else
                {
                    let embed = new RichEmbed().setAuthor(msg.author.username, msg.author.avatarURL)
                    .setColor([255, 125, 15]).setTitle(`You have ${timeLeft} days left on your membership`).setDescription("To renew: <insert link>")
                    .setThumbnail(bot.user.avatarURL).setTimestamp(new Date());
                    msg.author.send(embed);
                }
            });
        }
        catch(err) { if(err) console.log(err); }
    }
}